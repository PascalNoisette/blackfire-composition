ansible-galaxy install -r requirements.yml -p .

ansible-playbook blackfire -i inventory/docker -e magento_project_name=trf

docker-compose restart fpm

docker-compose restart web

docker-compose exec fpm bash
	/etc/init.d/blackfire-agent restart
